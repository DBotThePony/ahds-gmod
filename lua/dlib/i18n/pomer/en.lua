
-- Copyright (C) 20XX DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

gui.pomer.menu.title = 'ПОМЕР Settings'
gui.pomer.menu.cvar.cl_pomer = 'Enable the addon'
gui.pomer.menu.cvar.cl_pomer_stopsound = 'Stop sounds when dying'
gui.pomer.menu.cvar.cl_pomer_sound = 'Play sound effect'
gui.pomer.menu.cvar.cl_pomer_text = 'Custom text'
gui.pomer.menu.cvar.cl_pomer_subtitles = 'Display "subtitles"'
gui.pomer.menu.cvar.cl_pomer_animation = 'Display cartoon animation'
gui.pomer.menu.cvar.cl_pomer_time_mult = 'Animation speed multiplier'

gui.pomer.menu.help.cl_pomer_text = 'If put anything into form below,\nit will be displayed instead on dying'
gui.pomer.menu.help.cl_pomer_col = 'Foreground color'
gui.pomer.menu.help.cl_pomer_col_bg = 'Background color'

gui.pomer.deathscreen.subtitle = '[ Flatlined ]'
gui.pomer.deathscreen.tip = 'PRESS %s TO GET REVENGE'
