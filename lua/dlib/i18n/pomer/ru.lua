
-- Copyright (C) 20XX DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

gui.pomer.menu.title = 'ПОМЕР'
gui.pomer.menu.cvar.cl_pomer = 'Включить аддон'
gui.pomer.menu.cvar.cl_pomer_stopsound = 'Останавливать звуки'
gui.pomer.menu.cvar.cl_pomer_sound = 'Играть звуковой эффект'
gui.pomer.menu.cvar.cl_pomer_text = 'Свой текст'
gui.pomer.menu.cvar.cl_pomer_subtitles = 'Отображать субтитры'
gui.pomer.menu.cvar.cl_pomer_animation = 'Проигрывать мультик'
gui.pomer.menu.cvar.cl_pomer_time_mult = 'Множитель скорости анимации'

gui.pomer.menu.help.cl_pomer_text = 'Любая фраза введеная ниже\nбудет отображена вместо стандартного текста'
gui.pomer.menu.help.cl_pomer_col = 'Цвет переднего плана'
gui.pomer.menu.help.cl_pomer_col_bg = 'Цвет заднего плана'

gui.pomer.deathscreen.subtitle = '[ ПОМЕР ]'
gui.pomer.deathscreen.tip = 'НАЖМИТЕ %s ЧТОБЫ ОТОМСТИТЬ' -- а вот система гмода ПОДДЕРЖИВАЕТ вставку биндов напрямую
