
-- Copyright (C) 20XX DBot

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
-- of the Software, and to permit persons to whom the Software is furnished to do so,
-- subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all copies
-- or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
-- INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
-- PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
-- FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
-- DEALINGS IN THE SOFTWARE.

local cl_pomer = CreateConVar('cl_pomer', '1', {FCVAR_ARCHIVE}, 'Enable the addon')
local cl_pomer_stopsound = CreateConVar('cl_pomer_stopsound', '1', {FCVAR_ARCHIVE}, 'stopsound when dying')
local cl_pomer_sound = CreateConVar('cl_pomer_sound', '1', {FCVAR_ARCHIVE}, 'Play death sound effect')
local cl_pomer_subtitles = CreateConVar('cl_pomer_subtitles', '1', {FCVAR_ARCHIVE}, 'Display "subtitles"')
local cl_pomer_animation = CreateConVar('cl_pomer_animation', '1', {FCVAR_ARCHIVE}, 'Display cartoon animation')
local cl_pomer_text = CreateConVar('cl_pomer_text', '', {FCVAR_ARCHIVE}, 'If this is anything but zero length string, it is displayed instead')

local cl_pomer_time_mult = CreateConVar('cl_pomer_time_mult', '1', {FCVAR_ARCHIVE}, 'Animation time multiplier. Can be overriden by server!')

local sv_pomer_time = CreateConVar('sv_pomer_time', '0', {FCVAR_NOTIFY, FCVAR_REPLICATED}, 'Force animation time')
local sv_pomer_time_mult = CreateConVar('sv_pomer_time_mult', '1', {FCVAR_NOTIFY, FCVAR_REPLICATED}, 'Animation time multiplier.')
local sv_pomer_text_override = CreateConVar('sv_pomer_text_override', '', {FCVAR_ARCHIVE, FCVAR_NOTIFY, FCVAR_REPLICATED}, 'For bastards who can\'t read and use "AHDS_OverrideText" clientside hook')

--186, 36, 30
local cl_pomer_col_r = CreateConVar('cl_pomer_col_r', '186', {FCVAR_ARCHIVE}, '')
local cl_pomer_col_g = CreateConVar('cl_pomer_col_g', '36', {FCVAR_ARCHIVE}, '')
local cl_pomer_col_b = CreateConVar('cl_pomer_col_b', '30', {FCVAR_ARCHIVE}, '')

--255, 36, 30
local cl_pomer_col_bg_r = CreateConVar('cl_pomer_col_bg_r', '255', {FCVAR_ARCHIVE}, '')
local cl_pomer_col_bg_g = CreateConVar('cl_pomer_col_bg_g', '36', {FCVAR_ARCHIVE}, '')
local cl_pomer_col_bg_b = CreateConVar('cl_pomer_col_bg_b', '30', {FCVAR_ARCHIVE}, '')

surface.DLibCreateFont('PomerFont', {
	font = 'Blender Pro',
	size = 80,
	weight = 800,
	extended = true,
	-- scanlines = 4,
})
surface.DLibCreateFont('PomerSubtitleFont', {
	font = 'Blender Pro',
	size = 12,
	weight = 800,
	extended = true
})
surface.DLibCreateFont('PomerTipFont', {
	font = 'Blender Pro',
	size = 12,
	weight = 400,
	extended = true
})

local mat_BlurX = Material("pp/blurx")
local mat_BlurY = Material("pp/blury")
local tex_Bloom1 = render.GetBloomTex1()

local function BlurRenderTarget(rt, sizex, sizey, passes)
	mat_BlurX:SetTexture('$basetexture', rt)
	mat_BlurY:SetTexture('$basetexture', tex_Bloom1)
	mat_BlurX:SetFloat('$size', sizex)
	mat_BlurY:SetFloat('$size', sizey)

	for i = 1, passes + 1 do
		render.PushRenderTarget(tex_Bloom1)
		render.SetMaterial(mat_BlurX)
		render.DrawScreenQuad()
		render.PopRenderTarget()

		render.PushRenderTarget(rt)
		render.SetMaterial(mat_BlurY)
		render.DrawScreenQuad()
		render.PopRenderTarget()
	end
end

local blur_rt
local pomer_mat
local rt_w, rt_h

local function InvalidateMaterialCache()
	if not cl_pomer:GetBool() then return end
	rt_w, rt_h = ScrW(), math.round(ScrH() / 3)
	blur_rt = GetRenderTarget('pomer_rt_' .. rt_w .. 'x' .. rt_h, rt_w, rt_h)

	pomer_mat = CreateMaterial('pomer_mat', 'UnlitGeneric', {
		-- ['$basetexture'] = 'models/debug/debugwhite', -- force to __error
		-- to eliminate half-pixel correction
		['$translucent'] = '0',
		['$color'] = '[1 1 1]',
		['$alpha'] = '1',
		['$nolod'] = '1',
	})

	pomer_mat:SetTexture('$basetexture', blur_rt)
end

hook.Add('InvalidateMaterialCache', 'ПОМЕР', InvalidateMaterialCache)
InvalidateMaterialCache()

local time_start, time_start_ratio, time_start_line = 0, 0, 0
local time_fade_line, time_fade_subtitles = 0, 0
local time_end_ratio, time_end_close, time_end_line, time_end_scissors, time_end = 0, 0, 0, 0, 0
local last_alive = true

local PlayVideo, StopVideo

local function reboot_anim()
	StopVideo()
	local mult = sv_pomer_time:GetBool() and sv_pomer_time_mult:GetFloat() or cl_pomer_time_mult:GetFloat()

	time_start = SysTime()
	-- time_start_ratio = time_start + 0.24 * mult
	-- time_end_ratio = time_start_ratio + 1.8 * mult
	-- time_end_close = time_end_ratio + 0.24 * mult
	-- time_start_line = time_end_close + 0.03 * mult
	-- time_end_line = time_start_line + 0.8 * mult
	-- time_fade_line = time_end_line + 0.6 * mult
	-- time_end_scissors = time_end_line + 1.6 * mult

	time_start_ratio = time_start + 0.24 * mult
	time_end_ratio = time_start_ratio + 1.8 * mult
	time_end_close = time_end_ratio + 0.24 * mult
	time_start_line = time_end_close + 0.03 * mult
	time_end_line = time_start_line + 0.8 * mult
	time_fade_line = time_end_line + 0.6 * mult
	time_end_scissors = time_end_line + 0.5 * mult
	time_fade_subtitles = time_end_scissors + 0.5 * mult
end

local bezier_func = {
	0, 0.1, 0.21, 0.45, 0.66, 0.74, 0.79, 0.82, 0.85, 0.88, 0.91, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.98, 0.98, 0.99, 0.99, 0.99, 1
}

local bezier_func_line = {
	0, 0.1, 0.21, 0.45, 0.66, 0.74, 0.79, 0.82, 0.88, 0.93, 0.96, 0.99, 1
}

hook.Add('AHDS_OverrideText', 'default server override', function(_)
	local text = sv_pomer_text_override:GetString():trim()
	if text ~= '' then return text end
end, math.huge)

local function draw_text(_w, _h)
	local text = cl_pomer_text:GetString()
	local reptext = hook.Run('AHDS_OverrideText', text)
	if text == '' then text = 'ПОМЕР' end
	if reptext == false then return 0, 0 end
	if reptext then text = reptext end

	local w, h = surface.GetTextSize(text)

	if time_end_scissors > SysTime() then
		local progress = math.tbezier(SysTime():progression(time_end_line, time_end_scissors), bezier_func) / 2

		render.SetScissorRect(0, 0, _w, _h / 2, true)
		surface.SetTextPos(_w / 2 - w / 2, _h / 2 - h * progress)
		surface.DrawText(text)

		render.SetScissorRect(0, _h / 2, _w, _h, true)
		surface.SetTextPos(_w / 2 - w / 2, _h / 2 - h + h * progress)
		surface.DrawText(text)

		render.SetScissorRect(0, 0, 0, 0, false)
		return w, h
	end

	surface.SetTextPos(_w / 2 - w / 2, _h / 2 - h / 2)
	surface.DrawText(text)

	return w, h
end

local function HUDShouldDraw(str)
	if str == 'CHudDamageIndicator' then return false end
end

local HUDPaint, CalcView
local last_stop_sound = false
local last_post_sound = false

local function Think()
	if not cl_pomer:GetBool() then return end

	local ply = DLib.HUDCommons.SelectPlayer()
	if ply:Team() == TEAM_CONNECTING then return end

	if last_alive ~= ply:Alive() then
		last_alive = ply:Alive()

		if not last_alive then
			reboot_anim()

			if cl_pomer_sound:GetBool() then
				Entity(0):EmitSound('A_SFX_FP_Death')
			end

			hook.Add('HUDShouldDraw', 'ПОМЕР', HUDShouldDraw, -2)
			hook.Add('HUDPaint', 'ПОМЕР', HUDPaint, 15)
			hook.Add('CalcView', 'ПОМЕР', CalcView, -1000)
		else
			last_stop_sound = false
			last_post_sound = false

			hook.Remove('HUDShouldDraw', 'ПОМЕР')
			hook.Remove('HUDPaint', 'ПОМЕР')
			hook.Remove('CalcView', 'ПОМЕР')

			Entity(0):StopSound('A_SFX_FP_Death')
			Entity(0):StopSound('A_SFX_FP_DeathPost')
			StopVideo()
		end
	end

	if not last_alive and time_end_close <= SysTime() then
		if not last_stop_sound and cl_pomer_stopsound:GetBool() then
			RunConsoleCommand('stopsound')
			last_stop_sound = true
		end

		if not last_post_sound then
			-- Entity(0):StopSound('A_SFX_FP_Death')

			DLib.SysTimer.Simple(0, function()
				Entity(0):EmitSound('A_SFX_FP_DeathPost')
			end)

			last_post_sound = true
		end
	end
end

function HUDPaint()
	if last_alive then return end
	if hook.Run('AHDS_ShouldDraw') == false then return end

	local sys_time = SysTime()

	if time_start_ratio > sys_time then
		surface.SetDrawColor(0, 0, 0)
		local mult = math.tbezier(sys_time:progression(time_start, time_start_ratio), bezier_func_line)
		surface.DrawRect(-4, -4, ScrW() + 4, ScrH() * 0.2 * mult)
		surface.DrawRect(-4, ScrH() * (1 - mult * 0.2), ScrW() + 4, ScrH() * 0.2 + 4)
		return
	elseif time_end_ratio > sys_time then
		surface.SetDrawColor(0, 0, 0)
		surface.DrawRect(-4, -4, ScrW() + 4, ScrH() * 0.2)
		surface.DrawRect(-4, ScrH() * 0.8, ScrW() + 4, ScrH() * 0.2 + 4)
		return
	elseif time_end_close > sys_time then
		surface.SetDrawColor(0, 0, 0)
		local mult = math.tbezier(sys_time:progression(time_end_ratio, time_end_close), bezier_func_line)
		surface.DrawRect(-4, -4, ScrW() + 4, ScrH() * (0.2 + mult * 0.3))
		surface.DrawRect(-4, ScrH() * (0.8 - mult * 0.3), ScrW() + 4, ScrH() / 2 + 4)
		return
	end

	surface.SetDrawColor(0, 0, 0)
	surface.DrawRect(-4, -4, ScrW() + 4, ScrH() + 4)
	local line_height = ScreenSize(0.75):max(2)

	if time_end_line > sys_time then
		surface.SetDrawColor(cl_pomer_col_r:GetInt(), cl_pomer_col_g:GetInt(), cl_pomer_col_b:GetInt())
		surface.DrawRect(0, ScrH() / 2 - line_height / 2, ScrW() * math.tbezier(sys_time:progression(time_start_line, time_end_line), bezier_func), line_height)

		return
	end

	surface.SetFont('PomerFont')

	surface.SetTextColor(cl_pomer_col_bg_r:GetInt(), cl_pomer_col_bg_g:GetInt(), cl_pomer_col_bg_b:GetInt())

	render.PushRenderTarget(blur_rt)
	render.Clear(0, 0, 0, 255, true, true)

	cam.Start2D()

	draw_text(rt_w, rt_h)

	cam.End2D()

	render.PopRenderTarget(blur_rt)

	BlurRenderTarget(blur_rt, 16, 16, 12)

	surface.SetTextColor(cl_pomer_col_r:GetInt(), cl_pomer_col_g:GetInt(), cl_pomer_col_b:GetInt())

	surface.SetMaterial(pomer_mat)
	surface.DrawTexturedRectUV(ScrW() / 2 - rt_w / 2, ScrH() / 2 - rt_h / 2, rt_w, rt_h, -0.016129032258065, -0.016129032258065, 1.0161290322581, 1.0161290322581)

	draw_text(ScrW(), ScrH())

	surface.SetDrawColor(0, 0, 0, 150)
	local __h = ScrH() / 2 - rt_h / 2
	w = ScrW()

	for i = 0, rt_h, 4 do
		surface.DrawRect(0, __h + i, w, 1)
	end

	if time_fade_line > sys_time then
		surface.SetDrawColor(cl_pomer_col_r:GetInt(), cl_pomer_col_g:GetInt(), cl_pomer_col_b:GetInt(), 255 - Quintic(sys_time:progression(time_end_line, time_fade_line)) * 255)
		surface.DrawRect(0, ScrH() / 2 - line_height / 2, ScrW(), line_height)
	end

	if time_end_scissors <= sys_time and cl_pomer_subtitles:GetBool() then
		local alpha = Quintic(sys_time:progression(time_end_scissors, time_fade_subtitles)) * 255

		surface.SetFont('PomerSubtitleFont')
		local text = DLib.I18n.Localize('gui.pomer.deathscreen.subtitle')
		local _sw, _sh = surface.GetTextSize(text)

		surface.SetTextColor(cl_pomer_col_r:GetInt(), cl_pomer_col_g:GetInt(), cl_pomer_col_b:GetInt(), alpha)
		surface.SetTextPos(ScrW() * .5 - _sw * .5, ScrH() * .625)
		surface.DrawText(text)

		surface.SetFont('PomerTipFont')
		text = DLib.I18n.Localize('gui.pomer.deathscreen.tip', input.LookupBinding('+jump', true))
		local _sw, _sh = surface.GetTextSize(text)

		surface.SetTextColor(255, 255, 255, alpha)
		surface.SetTextPos(ScrW() * .5 - _sw * .5, ScrH() * .8)
		surface.DrawText(text)
	end

	PlayVideo()
end

local nowhere = {
	origin = Vector(-32000, -32000, -32000),
	angles = Angle(),
	znear = 0.5,
	zfar = 10000
}

function CalcView()
	if last_alive or time_end_close > SysTime() or not cl_pomer_stopsound:GetBool() then return end

	return nowhere
end

local function PopulateToolMenu()
	spawnmenu.AddToolMenuOption('Utilities', 'User', 'gui.pomer.menu.title', 'gui.pomer.menu.title', '', '', function(self)
		self:CheckBox('gui.pomer.menu.cvar.cl_pomer', 'cl_pomer')
		self:CheckBox('gui.pomer.menu.cvar.cl_pomer_stopsound', 'cl_pomer_stopsound')
		self:CheckBox('gui.pomer.menu.cvar.cl_pomer_sound', 'cl_pomer_sound')
		self:CheckBox('gui.pomer.menu.cvar.cl_pomer_subtitles', 'cl_pomer_subtitles')
		self:CheckBox('gui.pomer.menu.cvar.cl_pomer_animation', 'cl_pomer_animation')
		self:NumSlider('gui.pomer.menu.cvar.cl_pomer_time_mult', 'cl_pomer_time_mult', 0.01, 4, 2)

		self:Help('gui.pomer.menu.help.cl_pomer_text')
		self:TextEntry('gui.pomer.menu.cvar.cl_pomer_text', 'cl_pomer_text')

		self:Help('gui.pomer.menu.help.cl_pomer_col')

		local picker = vgui.Create('DLibColorMixer', self)
		picker:SetConVarR('cl_pomer_col_r')
		picker:SetConVarG('cl_pomer_col_g')
		picker:SetConVarB('cl_pomer_col_b')
		picker:SetAllowAlpha(false)
		picker:Dock(TOP)
		picker:SetTallLayout(true)

		self:Help('gui.pomer.menu.help.cl_pomer_col_bg')

		picker = vgui.Create('DLibColorMixer', self)
		picker:SetConVarR('cl_pomer_col_bg_r')
		picker:SetConVarG('cl_pomer_col_bg_g')
		picker:SetConVarB('cl_pomer_col_bg_b')
		picker:SetAllowAlpha(false)
		picker:Dock(TOP)
		picker:SetTallLayout(true)
	end)
end

hook.Add('PopulateToolMenu', 'ПОМЕР', PopulateToolMenu)

local function handle_convar_change()
	if cl_pomer:GetBool() then
		InvalidateMaterialCache()
		hook.Add('Think', 'ПОМЕР', Think)
	else
		hook.Remove('HUDShouldDraw', 'ПОМЕР')
		hook.Remove('HUDPaint', 'ПОМЕР')
		hook.Remove('CalcView', 'ПОМЕР')
		hook.Remove('Think', 'ПОМЕР')

		if AreEntitiesAvailable() then
			Entity(0):StopSound('A_SFX_FP_Death')
			Entity(0):StopSound('A_SFX_FP_DeathPost')
		end

		StopVideo()
	end
end

cvars.AddChangeCallback('cl_pomer', handle_convar_change, 'ПОМЕР')
handle_convar_change()

sound.Add({
	name = "A_SFX_FP_Death",
	chan = CHAN_STATIC,
	level = 0,
	volume = 1,
	pitch = 100,
	sound = {
		")ahds/fp_character/death/death_pre0.ogg",
		")ahds/fp_character/death/death_pre1.ogg"
	}
})

sound.Add({
	name = "A_SFX_FP_DeathPost",
	chan = CHAN_STATIC,
	level = 0,
	volume = 1,
	pitch = 100,
	sound = {
		")ahds/fp_character/death/death_post.ogg"
	}
})

if BRANCH ~= "x86-64" then
	PlayVideo = function() end
	StopVideo = function() end

	return
end

local LastDmgType, HTMLVideoPlayer

local DmgTypeVideos = {
	[DMG_GENERIC] = "death_bitten",
	[DMG_ACID] = "death_acid",
	[DMG_CRUSH] = "death_bitten",
	[DMG_SLASH] = "death_bitten",
	[DMG_BLAST] = "death_blast",
	[DMG_SHOCK] = "death_electro",
	[DMG_FALL] = "death_fall",
	[DMG_BURN] = "death_fire",
	[DMG_ENERGYBEAM] = "death_laser",
	[DMG_BULLET] = "death_shot",
}

local function IsPlayerValid()
	if IsValid(HTMLVideoPlayer) then return true end

	if not _G.AHDS_HTML then
		_G.AHDS_HTML = vgui.Create("DHTML")
		_G.AHDS_HTML:Hide()
	end

	HTMLVideoPlayer = _G.AHDS_HTML

	return IsValid(HTMLVideoPlayer)
end

function PlayVideo()
	if not IsPlayerValid() then return end
	if HTMLVideoPlayer:IsVisible() then return end
	if not cl_pomer_animation:GetBool() then return end

	LastDmgType = LastDmgType or 0
	if not DmgTypeVideos[LastDmgType] then LastDmgType = nil return end

	local scale = ScrH() / 1080

	local w, h = 640 * scale, 360 * scale

	if HTMLVideoPlayer:GetWide() ~= w or HTMLVideoPlayer:GetTall() ~= h then
		HTMLVideoPlayer:SetSize(w, h)
	end
	HTMLVideoPlayer:SetPos(ScrW() * .5 - w * .5, 20 * scale)

	HTMLVideoPlayer:SetHTML([[
		<video id="videoplayer" width="100%" height="100%" autoplay preload="metadata">
			<source src="asset://garrysmod/materials/ahds/deathcartoons/]] .. DmgTypeVideos[LastDmgType] .. [[.webm.vtf" type="video/webm">
		</video>
		<script type="text/javascript">
			document.getElementById('videoplayer').volume = ]] .. GetConVar("volume"):GetFloat() .. [[
		</script>
	]])
	HTMLVideoPlayer:Show()

	LastDmgType = nil
end

function StopVideo()
	if not IsPlayerValid() then return end

	HTMLVideoPlayer:SetHTML("")
	HTMLVideoPlayer:Hide()
end

gameevent.Listen('entity_killed')
hook.Add('entity_killed', 'ПОМЕР', function(data)
	local ent = Entity(data.entindex_killed)
	if not IsValid(ent) or not ent:IsPlayer() then return end

	if ent == LocalPlayer() then
		local dmgtype = data.damagebits

		for t,v in pairs(DmgTypeVideos) do
			if bit.band(dmgtype, t) == t then
				LastDmgType = t
			end
		end
	end
end)